# week 1

## HTML
* Text
* Tables
* Forms
* Sections

## JS

## Object

* https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Object

## Function

* https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Function

## String

* https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/String

## Array

* https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array
* https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/prototype

## Events

* https://developer.mozilla.org/pt-BR/docs/Web/Events

## XHR

* https://developer.mozilla.org/pt-BR/docs/Web/API/XMLHTTPRequest

## CSS
* text and font
* border, margin, padding
* display
* class and id

## jQuery
* val(), text(), html()
* css()
* ajax

## Bootstrap
* Text
* Tables
* Forms
* Sections